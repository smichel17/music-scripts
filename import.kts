import util.*

import java.io.File
import kotlin.concurrent.thread

val importFolder = File(args[0])
val library = Library(args[1], args[2])
val threads = if (args.size == 4) args[3].toInt() else 1

val songs: MutableList<Song> = importFolder.walkTopDown().filter {
    it.isFile && it.isSong
}.map { file -> Song(file) }.toMutableList()

val total = songs.size
val lock = java.lang.Object()

for (id in 1..threads) {
    thread {
        process(id)
        println("Thread $id: Finished")
    }
}

tailrec fun process(id: Int) {
    val (song, progress) = synchronized(lock) {
        val last = songs.lastIndex
        if (last == -1) {
            return
        } else {
            Pair(songs.removeAt(last), total - songs.size)
        }
    }
    println("IMPORT: ($progress/$total) ${song.file}")
    library.addSong(song)
    process(id)
}
