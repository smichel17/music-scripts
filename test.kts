import util.*

import java.io.File
import kotlin.concurrent.thread

val importFolder = File(args[0])
val library = Library(args[1], args[2])
val delete = args.contains("--delete")

val songs: MutableList<Song> = importFolder.walkTopDown().filter {
    it.isFile && it.isSong
}.map { file -> Song(file) }.toMutableList()

val lock = java.lang.Object()

for (id in 1..7) {
    thread {
        process(id)
        println("$id: Finished")
    }
}

tailrec fun process(id: Int) {
    val song = synchronized(lock) {
        val last = songs.lastIndex
        if (last == -1) {
            return
        } else {
            songs.removeAt(last)
        }
    }
    println("$id: ${song.file}")
    process(id)
}
