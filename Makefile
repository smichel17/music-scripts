#####################################################
#                     Options                       #
#####################################################

THREADS := 4

IMPORT := /music/cds/

LIBRARY := /music/flacs/
LFORMAT := flac

MOBILE := /music/mp3s/
MFORMAT := mp3

#####################################################
#                  Housekeeping                     #
#####################################################

KVER:= 1.2.10
JAUDIOTAGGER:= jaudiotagger-2.2.6-SNAPSHOT.jar

depdir:= build/
jtag:= $(depdir)$(JAUDIOTAGGER)
kc:= $(depdir)kotlinc/bin/kotlinc

empty:=
space:= $(empty) $(empty)
args:=  

#####################################################
#                  Main commands                    #
#####################################################

.PHONY: help setup build test import compress clean clobber

help:
	@echo 'Available commands:'
	@echo '  setup:    Download dependencies: kotlin compiler, jaudiotagger library'
	@echo '  build:    Compile utils'
	@echo '  test:     Does a dry run of import'
	@echo '  import:   Imports from $(IMPORT) to $(LIBRARY) in $(LFORMAT) format'
	@echo '  compress: Imports from $(LIBRARY) to $(MOBILE) in $(MFORMAT) format'
	@echo '  clean:    Delete jar files'
	@echo '  clobber:  Also delete $(depdir) folder'
	@echo '  help:     Print this message (default)'
	@echo ''
	@echo 'Edit the Makefile to change these parameters.'

setup: $(kc) $(jtag)

build: util.jar

test: util.jar $(jtag) | $(kc)
	$(kc) -classpath $(subst $(space),:,$^) -script \
		test.kts $(LIBRARY) $(MOBILE) $(MFORMAT) $(THREADS)

import: util.jar $(jtag) | $(kc)
	$(kc) -classpath $(subst $(space),:,$^) -script \
		import.kts $(IMPORT) $(LIBRARY) $(LFORMAT) $(THREADS)

compress: util.jar $(jtag) | $(kc)
	$(kc) -classpath $(subst $(space),:,$^) -script \
		import.kts $(LIBRARY) $(MOBILE) $(MFORMAT) $(THREADS)

clean:
	rm *.jar

clobber: clean
	rm -rf build/

#####################################################
#                   Dependencies                    #
#####################################################

%.jar: %.kt $(jtag) | $(kc)
	$(kc) $< -d $@ -cp $(jtag)

$(jtag): | $(depdir)
	@rm -f $@
	wget https://bitbucket.org/ijabz/jaudiotagger/downloads/$(JAUDIOTAGGER)
	mv $(JAUDIOTAGGER) $(jtag)

$(kc): | $(depdir)
	@rm -f kotlin-compiler-$(KVER).zip
	@rm -rf $(depdir)kotlinc/
	wget https://github.com/JetBrains/kotlin/releases/download/v$(KVER)/kotlin-compiler-$(KVER).zip
	unzip kotlin-compiler-$(KVER).zip
	mv kotlinc/ $(depdir)kotlinc
	rm -f kotlin-compiler-$(KVER).zip

$(depdir):
	mkdir $@
