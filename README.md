This is a script for managing my music collection. It should work cross-platform
on any system that has `make`, `wget`, `unzip`, `mv`, `rm`, and a JVM
implementation, although it has only been tested on Fedora 27 so far.

It is intended to be used via the Makefile, and the documentation for its usage
can be found inline there -- just clone the repo and run `make` (or `make help`,
they're the same) to get started.
