package util

import java.io.File
import java.io.PrintStream
import java.lang.ProcessBuilder

import org.jaudiotagger.audio.AudioFile
import org.jaudiotagger.audio.AudioFileIO
import org.jaudiotagger.audio.mp3.MP3File
import org.jaudiotagger.tag.FieldKey 
import org.jaudiotagger.tag.Tag

data class Library(val root: File, val format: String) {
    constructor(path: String, ext: String) : this(File(path), ext)
}

fun Library.addSong(song: Song) {
    val file = song.file
    song.metadata.run {
        val artistFolder = File(root, albumArtist)
        val year = date.take(4)
        val albumName = album.sanitize()
        val songName = title.sanitize()
        val albumFolder = File(artistFolder, "($year) $albumName")
        val dest = File(albumFolder, "$track - $songName.$format")

        albumFolder.mkdirs()

        if (dest.exists()) {
            println("Skipping import, already exists: $dest")
        } else {
            val destSong = Song(dest)
            val args = listOf<String>("ffmpeg", "-i", file.path) + destSong.format.args + dest.path
            println("$ $args")
            ProcessBuilder(*args.toTypedArray()).start().waitFor()
            destSong.metadata = this
        }
    }
}

fun String.sanitize(): String {
    return this.replace('/', '-')
}

data class Metadata(
        val title: String,
        val artist: String,
        val albumArtist: String,
        val album: String,
        val track: String,
        val trackTotal: String,
        val date: String,
        val genre: String
)

class Song(val file: File) {
    val format = Format.valueOf(file.extension.toLowerCase())

    var metadata: Metadata
        get() = format.getMetadata(file).normalize()
        set(value) = format.setMetadata(file, value.normalize())
}

fun Metadata.normalize(): Metadata {
    return Metadata(
            title.or("Untitled"),
            artist.or(albumArtist.or("Unknown")),
            albumArtist.or(artist.or("Unknown")),
            album.or("Unknown"),
            track.or("00").padStart(2, '0'),
            trackTotal.or(track.or("00")).padStart(2, '0'),
            date.or("0000"),
            genre.or("Unknown")
    )
}

private fun String?.or(backup: String): String {
    return if (this.isNullOrBlank()) backup else this!!
}

val File.isSong: Boolean get() {
    val ext = this.extension.toLowerCase()
    return Format.values().map { it.name }.contains(ext)
}

enum class Format {
    flac {
        override fun getMetadata(file: File): Metadata {
            val tag = AudioFileIO.read(file).tag
            val artistFolder = file.parentFile.parentFile
            val albumArtist: String = artistFolder.name.replace('_', ' ')
            return Metadata(
                    tag.getFirst(FieldKey.TITLE),
                    tag.getFirst(FieldKey.ARTIST),
                    when (albumArtist) {
                        "Various Artists", "Moik Mix", "Unknown" -> albumArtist
                        else -> tag.getFirst(FieldKey.ALBUM_ARTIST)
                    },
                    tag.getFirst(FieldKey.ALBUM),
                    tag.getFirst(FieldKey.TRACK),
                    tag.getFirst(FieldKey.TRACK_TOTAL),
                    tag.getFirst(FieldKey.YEAR),
                    tag.getFirst(FieldKey.GENRE)
            )
        }

        override fun setMetadata(file: File, metadata: Metadata) {
            val f = AudioFileIO.read(file)
            val t = f.tag
            metadata.run {
                t.setField(FieldKey.TITLE, title)
                t.setField(FieldKey.ARTIST, artist)
                t.setField(FieldKey.ALBUM_ARTIST, albumArtist)
                t.setField(FieldKey.ALBUM, album)
                t.setField(FieldKey.TRACK, track)
                t.setField(FieldKey.TRACK_TOTAL, trackTotal)
                t.setField(FieldKey.YEAR, date)
                t.setField(FieldKey.GENRE, genre)
            }
            AudioFileIO.write(f)
        }
    },

    wav {
        override fun getMetadata(file: File): Metadata {
            val albumFolder = file.parentFile
            val artistFolder = albumFolder.parentFile
            val genreFolder = artistFolder.parentFile
            val fileName = file.nameWithoutExtension.replace('_', ' ')

            val title = fileName.drop(3)
            val track = fileName.take(2)
            val album = albumFolder.name.replace('_', ' ')
            val albumArtist = artistFolder.name.replace('_', ' ')
            val artist = albumArtist
            val date = "0000"
            val trackTotal = albumFolder.listFiles().count {
                it.isFile && it.extension == file.extension
            }.toString()
            val genre = genreFolder.name.replace('_', ' ')

            return Metadata(title, artist, albumArtist, album, track, trackTotal, date, genre)
        }

        override fun setMetadata(file: File, metadata: Metadata) {
            println("Can't write wav metadata")
            throw Exception("Can't write wav metadata")
        }
    },

    mp3 {
        override fun getMetadata(file: File): Metadata {
            println("Don't know how to read mp3 metadata")
            throw Exception("Don't know how to read mp3 metadata")
        }

        override fun setMetadata(file: File, metadata: Metadata) {
            // Assume we're writing from a flac and ffmpeg will handle it
        }

        override val args: List<String> = listOf<String>("-q:a", "1")
    };

    abstract fun getMetadata(file: File): Metadata
    abstract fun setMetadata(file: File, metadata: Metadata)
    open val args: List<String> = listOf<String>()
}

// Suppress stdout and stderr
fun <T>silent(block: () -> T): T {
    val out = System.out
    val err = System.err
    val tmp = File.createTempFile("music", "log")
    val ps = PrintStream(tmp.outputStream())

    System.setErr(ps)
    System.setOut(ps)
    val result = block()
    System.setOut(out)
    System.setErr(err)
    return result
}
