/*
 * This file is a collection of things I wrote before cutting down the scope of
 * these scripts. Keeping it around in case I want to pick out parts later. To
 * someone just looking to *use* these scripts, everything here can be ignored.
 */

import org.jaudiotagger.audio.AudioFile
import org.jaudiotagger.audio.AudioFileIO
import org.jaudiotagger.tag.FieldKey 
import org.jaudiotagger.tag.Tag

fun Library.getMetadata(song: Song): Metadata {
    song.run {
        val artistFolder = File(root, artist)
        val albumFolder = File(artistFolder, "($year) $album")
        val dest = File(albumFolder, "$track - $title.$format")

        return if (dest.exists()) dest.readMetadataFromTags() else Metadata()
    }
}

fun Library.getSong(file: File): Song {
    val t = AudioFileIO.read(file).tag
    return file.readMetadataFromTags().toSong().copy(tags = t)
}

enum class Overwrite {
    YES, NO, METADATA
}

val File.format: Format get() = Format.valueOf(extension.toLowerCase())

fun Metadata.withBackup(backup: Metadata): Metadata {
    return backup.let {
        Metadata(
                title ?: it.title,
                artist ?: it.artist,
                albumArtist ?: it.albumArtist,
                album ?: it.album,
                track ?: it.track,
                trackTotal ?: it.trackTotal,
                date ?: it.date,
                genre ?: it.genre
        )
    }
}

fun List<String?>.selectOne(
        prompt: String,
        isBetter: (String, String) -> Boolean = { _, _ -> false }
): String? {
    val distinct = this.filterNotNull().filterNot { it.isBlank() }.distinct()
    val choices = mutableListOf<String>()
    distinct.forEach { item ->
        val rest = distinct - item
        if (!rest.any { isBetter(it, item) }) {
            choices.add(item)
        }
    }
    return when (choices.size) {
        0 -> null
        1 -> choices.single()
        else -> {
            println(prompt)
            choices.forEachIndexed { index, item ->
                println("   ${index+1}. $item")
            }
            val answer = readLine()?.toIntOrNull() ?: -1
            if (answer in 1..choices.size) {
                choices[answer-1] 
            } else {
                println()
                choices.selectOne("Invalid option, please try again:")
            }
        }
    }
}

fun oldImport() {
    val pathmeta = file.readMetadataFromPath(importFolder)
    val tagmeta = silent { file.readMetadataFromTags() }
    val guess = tagmeta.withBackup(pathmeta).toSong()
    val libmeta = library.getMetadata(guess)

    val m = listOf<Metadata>(tagmeta, pathmeta, libmeta)

    val song = Metadata(
            m.map { it.title }.selectOne("Select a title:"),
            m.map { it.artist }.selectOne("Select a artist:"),
            m.map { it.albumArtist }.selectOne("Select a albumArtist:"),
            m.map { it.album }.selectOne("Select a album:"),
            m.map { it.track }.selectOne("Select a track:"),
            m.map { it.trackTotal }.selectOne("Select a trackTotal:"),
            m.map { it.date }.selectOne("Select a date:") { date1, date2 -> date2.startsWith(date1) },
            m.map { it.genre }.selectOne("Select a genre:")
    ).toSong()

    library.addSong(song, file)
}

fun printDebugInfo(file: File, path: File) {
    println("==========================================")
    println("==             FILE INFO                ==")
    println("==========================================")
    file.run {
        println("file: $file")
        println("name: $name")
        println("path: $path")
        println("absolutePath: $absolutePath")
        println("extension: $extension")
        println("nameWithoutExtension: $nameWithoutExtension")
    }

    println()
    println("------------------------------------------")
    println("--                RAW                   --")
    println("------------------------------------------")
    val fields = silent { AudioFileIO.read(file).tag.fields }
    fields.forEach { field -> println("${field.id}: $field") }

    println()
    
    println("------------------------------------------")
    println("--               PARSED                 --")
    println("------------------------------------------")
    val songFromMetaData = silent { file.readMetadataFromTags().toSong() }
    println("$songFromMetaData")

    println()

    println("------------------------------------------")
    println("--              FROM PATH               --")
    println("------------------------------------------")
    val songFromPath = file.readMetadataFromPath(path).toSong()
    println("$songFromPath")
    println()
}

//val albumNameRegex = Regex("""\([0-9]{4}\) .*""")
// val fileNameRegex = Regex("""[0-9]{2}-.*""")
